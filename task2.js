/**
 * Задание 2
 * Написать скрипт который наделяет элемент с атрибутом dragAndDrop возможностью перемещаться по экрану
 * по алгоритму Drag and drop.
 **/

/**
 * Алгоритм
 *
 * 1. Отследить событие mousedown на перетаскиваемом элементе
 *
 * 2. При mousemove - задавать элементу новые координаты
 *
 * 3. При mouseup - "отпустить" элемент  (удалить ненужные обработчики)
 *
 * 4. "Запомнить" положение курсора относительно элемента
 *    (подсказка: можно использовать .getBoundingClientRect();)
 * 5. Передвигать элемент в рамках определенного контейнера
 *    (подсказка: необходимо задать минимальное и максимально перемещение по осям X и Y)
 **/
let sun = document.getElementById('sun');
let box = document.getElementById('box');
let isDrag = false;
let limits = {
    top: box.offsetTop,
    right: box.offsetWidth + box.offsetLeft - sun.offsetWidth,
    bottom: box.offsetHeight + box.offsetTop - sun.offsetHeight,
    left: box.offsetLeft
  };

document.addEventListener('mousedown', event => {
    const sun = event.target;
    if (sun.hasAttribute('dragAndDrop')) {
        sun.style.position = 'absolute';
        sun.style.zIndex = '1000';

        // перемещение элемента
        const onMouseMove = event => {
            moveAt(event.pageX, event.pageY, sun);
        }
        // удаление неиспользуемых (больше) обработчиков
        const onMouseUp = () => {
            document.removeEventListener('mousemove', onMouseMove);
            sun.removeEventListener('mouseup', onMouseUp);
        }

        document.addEventListener('mousemove', onMouseMove);
        sun.addEventListener('mouseup', onMouseUp);
    }
});

    sun.onmousedown = function(e) {
    isDrag = true;
  }
  document.onmouseup = function() {
    isDrag = false;
  }
  document.onmousemove = function(e) {
    if (isDrag) {
      move(e);
    }
  }

function move(e) {
    let moveAt = {
      x: limits.left,
      y: limits.top
    };
    if (e.pageX > limits.right) {
      moveAt.x = limits.right;
    } else if (e.pageX > limits.left) {
      moveAt.x = e.pageX;
    }
    if (e.pageY > limits.bottom) {
      moveAt.y = limits.bottom;
    } else if (e.pageY > limits.top) {
      moveAt.y = e.pageY;
    }
    relocate(moveAt);
}

function relocate(moveAt) {
    sun.style.left = moveAt.x + 'px';
    sun.style.top = moveAt.y + 'px';
}


